from boto.s3.connection import S3Connection
from boto.s3.key import Key
import mysql.connector
from settings import *
import datetime


def getLastBackup(conn, bucket_name, backupType):
    bucket = conn.get_bucket(bucket_name)
    files = bucket.list(backupType)
    tmp = {}
    for f in files:
        if f.name.endswith(".gz"):
            tmp.update({f.name: f.last_modified})
    last = sorted(tmp, key=tmp.get, reverse=True)[0]
    ret = {'backup_date': tmp[last],
           'backup_name': last,
           }
    return ret


def getS3Creds(cur, cust_short):
    query = "SELECT access_key,access_secret,bucket_name FROM s3_info WHERE cust_id = (SELECT cust_id FROM customer WHERE cust_short LIKE '%s')" % (
        cust_short)
    cur.execute(query)
    tmp_creds = cur.fetchone()
    creds = {}
    has_civi = checkCivicrm(cursor, cust_short)
    creds.update({'access_key': tmp_creds[0],
                  'access_secret': tmp_creds[1],
                  'bucket_name': tmp_creds[2],
                  'cust_short': cust_short,
                  'has_civicrm': has_civi,
                  })
    return creds


# downloads latest backup file
def getBackup(conn, bucket_name, backup_name, creds):
    bucket = conn.get_bucket(bucket_name)
    k = Key(bucket, backup_name)
    filename = "dumps/" + backup_name.split('/')[1]
    print "saving backup in file: %s" % filename
    with open(filename, "wb") as f:
        k.get_contents_to_file(f)
        f.truncate()
    return filename


# insert latest dump locations into database
def updateBackupDB(cur, latest_backup, latest_backup_date, cust_short, backup_type):
    query = ""
    if backup_type == "drupal_daily":
        query = "UPDATE customer SET latest_path_drupal = '%s',latest_dump_date = '%s'  WHERE cust_short  LIKE '%s'" % (
            latest_backup, latest_backup_date.strftime('%Y-%m-%d %H:%M:%S'), cust_short)
    if backup_type == "civicrm_daily":
        query = "UPDATE customer SET latest_path_civicrm = '%s',latest_dump_date = '%s'  WHERE cust_short  LIKE '%s'" % (
            latest_backup, latest_backup_date.strftime('%Y-%m-%d %H:%M:%S'), cust_short)
    print query
    cur.execute(query)


def downloadBackup(creds, backup_type, cursor):
    conn = S3Connection(creds["access_key"], creds["access_secret"])
    last_backup = getLastBackup(conn, creds['bucket_name'], backup_type)
    # download latest backup file and return its path
    latest_backup = getBackup(conn, creds["bucket_name"], last_backup["backup_name"], creds)
    # update db with latest dump
    latest_backup_date = datetime.datetime.now()
    backup = {}
    backup.update({
        "latest_backup_date": latest_backup_date,
        "latest_backup": latest_backup,
        "backup_type": backup_type,

    })
    return backup


def checkCivicrm(cursor, cust_short):
    query = "select has_civicrm from customer where cust_id = (SELECT cust_id FROM customer WHERE cust_short LIKE '%s')" % cust_short
    cursor.execute(query)
    res = cursor.fetchone()
    return res[0]


def getBackupDirs(cursor, cust_short):
    query = "select backup_dirs from customer where cust_id = (SELECT cust_id FROM customer WHERE cust_short LIKE '%s')" % cust_short
    cursor.execute(query)
    res = cursor.fetchone()
    backup_dirs = res[0].split(',')
    return backup_dirs


cnx = mysql.connector.connect(**config)
cursor = cnx.cursor()
creds = getS3Creds(cursor, "compucorp")
print creds
backup_dirs = getBackupDirs(cursor, "compucorp")
for backup_dir in backup_dirs:
    tmp = downloadBackup(creds, backup_dir, cursor)
    updateBackupDB(cursor, tmp["latest_backup"], tmp["latest_backup_date"], creds["cust_short"], tmp["backup_type"])
cnx.commit()
cnx.close()
