UPDATE users
INNER JOIN users AS e2 ON users.uid = e2.uid
SET users.mail = CONCAT( e2.uid ,'@','example.com' ), users.init = CONCAT( e2.uid ,'@','example.com' )
WHERE users.uid = e2.uid;
UPDATE users AS e1
INNER JOIN users AS e2
ON e1.uid = e2.uid
SET e1.name = CONCAT('Anonymous', e2.uid)
WHERE e1.uid != 0 AND e1.uid NOT IN (SELECT DISTINCT uid FROM users_roles ur LEFT JOIN role r ON ur.rid = r.rid WHERE r.name = 'administrator');
Update url_alias AS u1 INNER JOIN url_alias AS u2 on u1.pid = u2.pid SET u1.alias = CONCAT('users/anonymous',SUBSTRING_INDEX(u2.source,'/', -1)) where u1.pid = u2.pid AND u1.source like 'user%';
UPDATE webform_submitted_data SET data = 'Anonymous';
UPDATE field_data_commerce_customer_address SET commerce_customer_address_administrative_area = 'Anonymous', commerce_customer_address_sub_administrative_area = 'Anonymous', commerce_customer_address_locality = 'Anonymous',
commerce_customer_address_dependent_locality = 'Anonymous', commerce_customer_address_postal_code = 'Anonymous', commerce_customer_address_postal_code = 'Anonymous', commerce_customer_address_thoroughfare = 'Anonymous',
commerce_customer_address_premise = 'Anonymous', commerce_customer_address_sub_premise = 'Anonymous', commerce_customer_address_organisation_name = 'Anonymous', commerce_customer_address_name_line = 'Anonymous',
commerce_customer_address_first_name = 'Anonymous', commerce_customer_address_last_name = 'Anonymous', commerce_customer_address_data = 'Anonymous';
UPDATE field_revision_commerce_customer_address SET commerce_customer_address_administrative_area = 'Anonymous', commerce_customer_address_sub_administrative_area = 'Anonymous', commerce_customer_address_locality = 'Anonymous',
commerce_customer_address_dependent_locality = 'Anonymous', commerce_customer_address_postal_code = 'Anonymous', commerce_customer_address_postal_code = 'Anonymous', commerce_customer_address_thoroughfare = 'Anonymous',
commerce_customer_address_premise = 'Anonymous', commerce_customer_address_sub_premise = 'Anonymous', commerce_customer_address_organisation_name = 'Anonymous', commerce_customer_address_name_line = 'Anonymous',
commerce_customer_address_first_name = 'Anonymous', commerce_customer_address_last_name = 'Anonymous', commerce_customer_address_data = 'Anonymous';
truncate cache;
truncate cache_admin_menu;
truncate cache_block;
truncate cache_bootstrap;
truncate cache_features;
truncate cache_field;
truncate cache_filter;
truncate cache_form;
truncate cache_image;
truncate cache_libraries;
truncate cache_menu;
truncate cache_page;
truncate cache_path;
truncate cache_rules;
truncate cache_token;
truncate cache_update;
truncate cache_views;
truncate cache_views_data;
truncate user_import_errors;
truncate uc_orders;
truncate uc_payment_paypal_ipn;
truncate uc_payment_other;
truncate uc_order_log;
truncate watchdog;
DELETE FROM  backup_migrate_destinations;
DELETE FROM  backup_migrate_profiles;
DELETE FROM  backup_migrate_schedules;
DELETE FROM  backup_migrate_sources;
UPDATE system SET status='0' WHERE name='backup_migrate';
UPDATE users
INNER JOIN users AS e2 ON users.uid = e2.uid
SET users.mail = CONCAT( e2.uid ,'@','example.com' ), users.init = CONCAT( e2.uid ,'@','example.com' )
WHERE users.uid = e2.uid;
UPDATE system SET status='0' WHERE name='backup_migrate';
DELETE FROM  backup_migrate_destinations;
DELETE FROM  backup_migrate_profiles;
DELETE FROM  backup_migrate_schedules;
DELETE FROM  backup_migrate_sources;