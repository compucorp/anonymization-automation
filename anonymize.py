import mysql.connector
from random import choice
from string import ascii_lowercase
from settings import *
import gzip
import os


def databaseApply(creds, cur):
    # create civicrm db
    create_database = "CREATE DATABASE %s" % (creds["civicrm_db"])
    cur.execute(create_database)

    # create drupal db
    create_database = "CREATE DATABASE %s" % (creds["drupal_db"])
    cur.execute(create_database)

    # create new user
    add_user = "CREATE USER %s@localhost identified by \'%s\'" % (creds["user"], creds["pass"])
    cur.execute(add_user)

    # add privileges to the user on newly created dbs
    grant_privileges = "GRANT ALL PRIVILEGES ON %s.* TO \'%s\'@\'localhost\'" % (creds["drupal_db"], creds["user"])
    cur.execute(grant_privileges)
    grant_privileges = "GRANT ALL PRIVILEGES ON %s.* TO \'%s\'@\'localhost\'" % (creds["civicrm_db"], creds["user"])
    cur.execute(grant_privileges)
    cur.execute("FLUSH PRIVILEGES")


def storeCreds(creds, cur):
    query = "SELECT cust_id FROM customer WHERE cust_short LIKE \'%s\'" % creds["cust_short"]
    cur.execute(query)
    cust_id = cur.fetchone()
    cust_id = int(cust_id[0])
    query = "INSERT INTO dbs(user,pass,db_drupal,db_civicrm ,cust_id) VALUES(\'%s\',\'%s\',\'%s\',\'%s\',%d)" % (
    creds["user"], creds["pass"], creds["drupal_db"], creds["civicrm_db"], cust_id)
    cur.execute(query)


def generateNewCreds(cust_short):
    tmp_random = (''.join(choice(ascii_lowercase) for i in range(8)))
    creds = {}
    tmp_civicrm = cust_short + "_" + tmp_random + "_civicrm"
    tmp_drupal = cust_short + "_" + tmp_random + "_drupal"
    tmp_user = "user_" + tmp_random
    creds.update({'user': tmp_user,
                  'pass': tmp_random,
                  'drupal_db': tmp_drupal,
                  'civicrm_db': tmp_civicrm,
                  'cust_short': cust_short,
                  })
    return creds


def importDB(cur, creds):
    query = "SELECT latest_path_drupal,latest_path_civicrm FROM customer WHERE cust_short LIKE '%s'" % creds[
        "cust_short"]
    cur.execute(query)
    query_result = cur.fetchone()
    latest_path_drupal = query_result[0]
    latest_path_civicrm = query_result[1]
    latest_path_civicrm = unzipDB(latest_path_civicrm)
    latest_path_drupal = unzipDB(latest_path_drupal)
    statement = "mysql -uroot -p%s '%s' < %s" % (config["password"], creds["civicrm_db"], latest_path_civicrm)
    print statement
    os.system(statement)
    statement = "mysql -uroot -p%s '%s' < %s" % (config["password"], creds["drupal_db"], latest_path_drupal)
    print statement
    os.system(statement)


def anonymizeDB(creds):
    conn = mysql.connector.connect(user=creds["user"], password=creds["pass"], database=creds["drupal_db"])
    cursor = conn.cursor()
    fd = open('drupal.sql')
    file_tmp = fd.read()
    sql_commands = file_tmp.split(';')
    for sql_command in sql_commands:
        try:
            cursor.execute(sql_command)
        except:
            pass
    cursor.close()
    conn.commit()
    conn.close()

    conn = mysql.connector.connect(user=creds["user"], password=creds["pass"], database=creds["civicrm_db"])
    cursor = conn.cursor()
    fd = open('civicrm.sql')
    file_tmp = fd.read()
    sql_commands = file_tmp.split(';')
    for sql_command in sql_commands:
        try:
            cursor.execute(sql_command)
        except:
            pass
    cursor.close()
    conn.commit()
    conn.close()


def unzipDB(backup_path):
    inFile = gzip.GzipFile(backup_path, 'rb')
    s = inFile.read()
    inFile.close()
    backup_path_new = backup_path[:-3]
    outFile = file(backup_path_new, 'wb')
    outFile.write(s)
    outFile.close()
    return backup_path_new


# establish mysql connection
cnx = mysql.connector.connect(**config)
cur = cnx.cursor()
creds = generateNewCreds("compucorp")
databaseApply(creds, cur)
storeCreds(creds, cur)
importDB(cur, creds)
anonymizeDB(creds)
cur.close()
cnx.commit()
cnx.close()
print creds