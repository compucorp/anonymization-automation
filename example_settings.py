config = {
  'user': '<db username>',
  'password': '<db_password>',
  'host': '127.0.0.1',
  'database': 'anon',
  'raise_on_warnings': True,
}